﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Npgsql;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WorkshopDocker
{
	[Route("test")]
	public class TestController : Controller
	{
		private readonly TestConnectionString _configuration;

		public TestController(TestConnectionString configuration)
		{
			_configuration = configuration;
		}

		// GET: /<controller>/
		public IActionResult Test()
		{
			try
			{
				using (var conn = new NpgsqlConnection(_configuration.ConnectionString))
				{
					conn.Open();
					using (var tran = conn.BeginTransaction())
					{
						conn.Query("Select 1;");
						return new JsonResult("Удалось подключиться к бд");
					}
				}
			}
			catch (Exception e)
			{
				return new JsonResult("НЕ удалось подключиться к бд");
			}
			


		
		}
	}

	public class TestConnectionString
	{
		public string ConnectionString { get; }

		public TestConnectionString(string connectionString)
		{
			ConnectionString = connectionString;
		}
	}
}
